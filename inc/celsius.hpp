#ifndef CELSIUS_HPP
#define CELSIUS_HPP
#include "medidortemperatura.hpp"

using namespace std;

class Celsius : public MedidorTemperatura {
public:
  float converterKelvin(float temperatura);
  float converterFahrenheit(float temperatura);
};

#endif
