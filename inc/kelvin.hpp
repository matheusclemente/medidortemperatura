#ifndef KELVIN_HPP
#define KELVIN_HPP
#include "medidortemperatura.hpp"

using namespace std;

class Kelvin : public MedidorTemperatura {
public:
  float converterCelsius(float temperatura);
  float converterFahrenheit(float temperatura);
};

#endif
