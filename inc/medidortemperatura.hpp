#ifndef MEDIDORTEMPERATURA_HPP
#define MEDIDORTEMPERATURA_HPP

using namespace std;

class MedidorTemperatura{
private:
  float temperatura;

public:
  MedidorTemperatura(float temperatura);
  void setTemperatura(float temperatura);
  float getTemperatura();

};

#endif
