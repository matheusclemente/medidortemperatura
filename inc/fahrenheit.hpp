#ifndef FAHRENHEIT_HPP
#define FAHRENHEIT_HPP
#include "medidortemperatura.hpp"

using namespace std;

class Fahrenheit : public MedidorTemperatura {
public:
  float converterKelvin(float temperatura);
  float converterCelsius(float temperatura);
};

#endif
