#include "kelvin.hpp"

float Kelvin::converterCelsius(float temperatura){
  return (temperatura - 273.0);
}

float Kelvin::converterFahrenheit(float temperatura){
  return ((temperatura - 273.0)*9.0/5.0) + 32.0;
}
