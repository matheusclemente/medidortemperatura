#include "celsius.hpp"

using namespace std;

float Celsius::converterKelvin(float temperatura){
  return temperatura + 273.0;
}

float Celsius::converterFahrenheit(float temperatura){
  return ((temperatura - 32.0) * 5.0)/9.0;
}
