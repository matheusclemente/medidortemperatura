#include "fahrenheit.hpp"

using namespace std;

float Fahrenheit::converterKelvin(float temperatura){
  return ((temperatura - 32.0) * 5.0 / 9.0) + 273.0;
}

float Fahrenheit::converterCelsius(float temperatura){
  return (temperatura * 9.0 /5.0) + 32.0;
}
