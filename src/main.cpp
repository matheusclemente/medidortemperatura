#include <iostream>
#include "medidortemperatura.hpp"
#include "celsius.hpp"
#include "fahrenheit.hpp"
#include "kelvin.hpp"

using namespace std;

int main(int argc, char const *argv[]) {
  float temperatura;
  int escala;
  cout << "Digite o valor da temperatura:" << endl;
  cin >> temperatura;
  cout << "Defina a escala" << endl;
  cout << "1 - Celsius" << endl;
  cout << "2 - Fahrenheit" << endl;
  cout << "3 - Kelvin" << endl;
  cin >> escala;
  switch (escala) {
    case 1:
      Celsius * temperatura_1 = new MedidorTemperatura(temperatura);
      cout << "Temperatura:" << endl;
      cout << temperatura_1->getTemperatura() <<" ºC" << endl;
      cout << temperatura_1->converterFahrenheit(temperatura) <<" ºF" << endl;
      cout << temperatura_1->converterKelvin(temperatura) <<" K" << endl;
      break;
    case 2:
      Fahrenheit * temperatura_2 = new MedidorTemperatura(temperatura);
      cout << "Temperatura:" << endl;
      cout << temperatura_2->getTemperatura() <<" ºF" << endl;
      cout << temperatura_2->converterCelsius(temperatura) <<" ºC" << endl;
      cout << temperatura_2->converterKelvin(temperatura) <<" K" << endl;
      break;
    case 3:
    Kelvin * temperatura_3 = new MedidorTemperatura(temperatura);
    cout << "Temperatura:" << endl;
    cout << temperatura_3->getTemperatura() <<" K" << endl;
    cout << temperatura_3->converterCelsius(temperatura) <<" ºC" << endl;
    cout << temperatura_3->converterFahrenheit(temperatura) <<" ºF" << endl;
    break;
  }
  return 0;
}
