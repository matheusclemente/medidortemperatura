#include "medidortemperatura.hpp"

using namespace std;

MedidorTemperatura::MedidorTemperatura(float temperatura){
  this->temperatura = temperatura;
}

void MedidorTemperatura::setTemperatura(float temperatura){
  this->temperatura = temperatura;
}

float MedidorTemperatura::getTemperatura(){
  return temperatura;
}
